<body>
    <a href="{{ route('todos.create') }}">Create Todo</a>
    <ul>
        @foreach ($todos as $todo)
            <li>
                <div>
                    {{ $todo->name }}
                </div> is {{ $todo->done == 0 ? 'not done yet.' : 'already done.' }}
                <a href="{{ route('todos.edit', ['id' => $todo->id]) }}">Update</a>
                <form action="{{ route('todos.destroy', ['id' => $todo->id]) }}" method="POST">
                    @csrf
                    @method('DELETE')

                    <button type="submit">Delete</button>
                </form>
            </li>
        @endforeach
    </ul>
</body>