<body>
    <form action="{{ route('todos.store') }}" method="POST">
        @csrf
        <label for="name">Name</label>
        <input type="text" name="name" id="name"/>
        <input type="radio" name="done" id="todo_done" value="1" >
        <label for="todo_done">Done</label>
        <input type="radio" name="done" id="todo_undone" value="0">
        <label for="todo_undone">Undone</label>

        <button type="submit">Create</button>
    </form>
</body>