<body>
    <form action="{{ route('todos.update', ['id' => $todo->id]) }}" method="POST">
        @csrf
        {{ method_field('PUT') }}
        <label for="name">Name</label>
        <input type="text" name="name" id="name" value="{{ $todo->name }}"/>
        <input type="radio" name="done" id="todo_done" value="1"{{ $todo->done == 0 ? '' : ' checked' }}>
        <label for="todo_done">Done</label>
        <input type="radio" name="done" id="todo_undone" value="0"{{ $todo->done == 0 ? ' checked' : '' }}>
        <label for="todo_undone">Undone</label>

        <button type="submit">Update</button>
    </form>
</body>